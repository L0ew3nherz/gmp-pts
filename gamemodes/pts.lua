require("include/utils");
big_require("include", true,"*");


function OnGamemodeInit()
	print("-------------------------");
	print("Public Testserver started");
	print("-------------------------");

	SetServerHostname("PTS");
	SetServerDescription("GMP - Public Testserver");
	SetGamemodeName("Instant 80");
	EnableNickname(1);
	EnableExitGame(1);
	EnableChat(1);
	Enable_OnPlayerUpdate(1);
end
