function initInstantMode(playerid)
	-- Attributes
	SetPlayerMagicLevel(playerid, 6);
	SetPlayerStrength(playerid, 200);
	SetPlayerDexterity(playerid, 200);
	SetPlayerMaxMana(playerid, 999999);
	SetPlayerMana(playerid, 999999);
	SetPlayerMaxHealth(playerid, 999999);
	SetPlayerHealth(playerid, 999999);
	-- Talents
	SetPlayerLevel(playerid, 20);
	SetPlayerLearnPoints(playerid, 25);
	SetPlayerSkillWeapon(playerid, SKILL_1H, 80);
	SetPlayerSkillWeapon(playerid, SKILL_2H, 80);
	SetPlayerSkillWeapon(playerid, SKILL_BOW, 80);
	SetPlayerSkillWeapon(playerid, SKILL_CBOW, 80);
	SetPlayerScience(playerid, SCIENCE_OPENING_LOCKS, 1);
	SetPlayerScience(playerid, SCIENCE_SNEAKING, 1);
	SetPlayerScience(playerid, SCIENCE_REGENERATE, 1);
	SetPlayerScience(playerid, SCIENCE_FIREMASTER, 1);
	SetPlayerScience(playerid, SCIENCE_THIEF, 1);
	SetPlayerScience(playerid, SCIENCE_SLOSH_BLADES, 1);
	SetPlayerScience(playerid, SCIENCE_CREATING_RUNES, 1);
	SetPlayerScience(playerid, SCIENCE_ALCHEMY, 1);
	SetPlayerScience(playerid, SCIENCE_COLLECTING_TROPHIES, 1);
	SetPlayerScience(playerid, SCIENCE_FOREIGNLANGUAGE, 1);
	SetPlayerScience(playerid, SCIENCE_WISP, 1);
	SetPlayerScience(playerid, SCIENCE_CUSTOM_TALENT_1, 1);
	SetPlayerScience(playerid, SCIENCE_CUSTOM_TALENT_2, 1);
	SetPlayerScience(playerid, SCIENCE_CUSTOM_TALENT_3, 1);
	SetPlayerAcrobatic(playerid, 1);
	-- Items
	GiveItem(playerid, "ITRW_ARROW", 1000);
	GiveItem(playerid, "ITRW_BOLT", 1000);
	-- Equipment
	EquipArmor(playerid, "ITAR_DJG_H");
	EquipMeleeWeapon(playerid, "ITMW_BELIARWEAPON_2H_20");
	EquipRangedWeapon(playerid, "ITRW_BOW_H_04");
	EquipHelmet(playerid, "ITAR_HAT_15");
	EquipBelt(playerid, "ItBe_Addon_Prot_Point");
	EquipItem(playerid, "ItRi_Prot_Total_01");
	EquipItem(playerid, "ItRi_Prot_Total_02");
	EquipItem(playerid, "ItAm_Prot_Edge_01");
	-- Other
	OnPlayerCommandText(playerid, "/spawnmonster Troll");
	EquipItem(playerid, "ITRU_SLEEP");
	EquipItem(playerid, "ITRU_SHRINK");
	EquipItem(playerid, "ITRU_LIGHTNINGFLASH");
	EquipItem(playerid, "ITSC_FIRERAIN");
end

print(debug.getinfo(1).source .. " has been loaded.");
