-- Draws
VersionDraw = CreateDraw(950, 7975, os.capture("git log -1 --pretty=oneline"), "Font_Old_10_White_Hi.tga", 255,255,255);
TimeDraw = CreateDraw(130, 7400, "00:00 Uhr", "Font_Old_10_White_Hi.tga", 255,255,255);
TickDraw = CreateDraw(130, 7600, "TICKS", "Font_Old_10_White_Hi.tga", 255,255,255);


function DestroyDraws(t)
	if type(t) == "number" then
		return
	end
	for i=1, #t do
		DestroyDraw(t[i]);
	end
end

function HideDraws(playerid, t)
	if type(t) == "number" then
		return
	end
	for i=1, #t do
		HideDraw(playerid, t[i]);
	end
end

function ShowDraws(playerid, t)
	for i=1, #t do
		ShowDraw(playerid, t[i]);
	end
end

function CreateStatDraws(playerid)
	local CharacterStats = {
	"NAME:         "..GetPlayerName(playerid),
	"GUILD:        "..GetPlayerGuild(playerid),
	"MAGIC CIRCLE: "..GetPlayerMagicLevel(playerid),
	"EXPERIENCE:   "..GetPlayerExperience(playerid),
	"NEXTLEVEL:    "..GetPlayerExperienceNextLevel(playerid),
	"STRENGTH:     "..GetPlayerStrength(playerid),
	"DEXTERITY:    "..GetPlayerDexterity(playerid),
	"MANA:         "..GetPlayerMana(playerid).."/"..GetPlayerMaxMana(playerid),
	"HEALTH:       "..GetPlayerHealth(playerid).."/"..GetPlayerMaxHealth(playerid),
	"SKILL_1H:     "..GetPlayerSkillWeapon(playerid, SKILL_1H),
	"SKILL_2H:     "..GetPlayerSkillWeapon(playerid, SKILL_2H),
	"SKILL_BOW:    "..GetPlayerSkillWeapon(playerid, SKILL_BOW),
	"SKILL_CBOW:   "..GetPlayerSkillWeapon(playerid, SKILL_CBOW),
	};
	return CharacterStats;
end

function MultiDraw(x, y, tStr)
	local d = {};
	local offset = 0;
	for i=1, #tStr do
		table.insert(d, CreateDraw(x, y+offset, tStr[i], "Font_Old_10_White_Hi.tga", 255,255,255));
		offset = offset + 100;
	end
	return d
end


print(debug.getinfo(1).source .. " has been loaded.");
