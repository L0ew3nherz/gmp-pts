function OnPlayerChangeAdditionalVisual(playerid, currBodyModel, currBodyTexture, currHeadModel, currHeadTexture,
                                                  oldBodyModel, oldBodyTexture, oldHeadModel, oldHeadTexture)
	SendMessageToAll(255,255,255,string.format("%s changed their visual from %d %s %d %s to %d %s %d %s",
                     GetPlayerName(playerid), currBodyModel, currBodyTexture, currHeadModel, currHeadTexture,
                                             oldBodyModel, oldBodyTexture, oldHeadModel, oldHeadTexture));
end

print(debug.getinfo(1).source .. " has been loaded.");
