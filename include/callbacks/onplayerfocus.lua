-- Here comes the HUD
-- display textures with draws that contain information about the user standing in front of you
function OnPlayerFocus(playerid, focusid)
	--local width = 1024;
	--local height = 4096;
	if focusid == -1 then
		if Player[playerid].statdraws ~= nil then
			HideDraws(playerid, Player[playerid].statdraws);
		end
		HideTexture(playerid, Textures["FOCUS_HIGHLIGHT"]);
		Player[playerid].target = nil;
	else
		Player[playerid].target = focusid;
		Player[focusid].watcher = playerid;
		ShowTexture(playerid, Textures["FOCUS_HIGHLIGHT"]);
		if Player[playerid].statdraws ~= nil then
			Player[playerid].statdraws = MultiDraw(7100, 4600, CreateStatDraws(focusid));
			ShowDraws(playerid, Player[playerid].statdraws);
		else
			DestroyDraws(Player[playerid].statdraws);
			Player[playerid].statdraws = nil;
		end
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
