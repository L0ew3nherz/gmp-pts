function OnPlayerUpdate(playerid)
	Player[playerid].x,Player[playerid].y,Player[playerid].z = GetPlayerPos(playerid);
	if Player[playerid].x and Player[playerid].y and Player[playerid].z then
		UpdatePlayerDraw(playerid, PlayerDraws[playerid][2], 130, 6600, "X: "..Player[playerid].x,
		"Font_Old_10_White_Hi.tga", 255,255,255);
		UpdatePlayerDraw(playerid, PlayerDraws[playerid][3], 130, 6800, "Y: "..Player[playerid].y,
		"Font_Old_10_White_Hi.tga", 255,255,255);
		UpdatePlayerDraw(playerid, PlayerDraws[playerid][4], 130, 7000, "Z: "..Player[playerid].z,
		"Font_Old_10_White_Hi.tga", 255,255,255);
		UpdatePlayerDraw(playerid, PlayerDraws[playerid][5], 130, 7200,
		"Angle: "..GetPlayerAngle(playerid).." (0x0 => "..GetPlayerAngleTo(playerid, 0, 0)..")",
		"Font_Old_10_White_Hi.tga", 255,255,255);
		UpdateMapMarker(playerid);
		if Player[playerid].compass ~= nil then
			UpdateCompass(playerid);
		else
			CreateCompass(playerid);
		end
	end
	UpdatePlayerDraw(playerid, PlayerDraws[playerid][6], 130, 6200, "Walkstyle: "..GetPlayerAnimationName(playerid),
	"Font_Old_10_White_Hi.tga", 255,255,255);
	if Player[playerid].lastbuttonpressed then
		UpdatePlayerDraw(playerid, PlayerDraws[playerid][7], 130, 6000, "Last button: "..Player[playerid].lastbuttonpressed,
		"Font_Old_10_White_Hi.tga", 255,255,255);
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
