--- OnServerWorldTime
-- triggers according to 'ms_per_minute' in gmp_server.ini
function OnServerWorldTime(_, _, newHour, newMinute)
	SetDrawText(TimeDraw, string.format("Time: %02d:%02d", newHour, newMinute));
end

print(debug.getinfo(1).source .. " has been loaded.");
