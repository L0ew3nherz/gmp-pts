function OnPlayerCommandText(playerid, cmdtext)
	local cmd, params = GetCommand(cmdtext);
	if cmd == "/" then
		local str = string.format(" \
		GMP Public Testserver 1.5.1 \
		Max Players: %s \
		Max Bots: %s \
		Max Slots: %s \
		Max Timers: %s \
		Max Draws: %s \
		Max Textures: %s \
		Max Sounds: %s \
		Max World Items: %s \
		",
		GetMaxPlayers(), GetMaxBots(), GetMaxSlots(), GetMaxTimers(), GetMaxDraws(),
		GetMaxTextures(), GetMaxSounds(), GetMaxWorldItems());
		SendPlayerMessage(playerid, 255,255,255, str);
	-- NPC --
	elseif cmd == "/createnpc" then
		local valid, npc = sscanf(params, "s");
		if valid == 1 then
			local npcid = CreateNPC(npc);
			SendMessageToAll(255,255,255,"NPC "..npcid.." spawned.");
			SetPlayerMaxHealth(npcid, 1000);
			SetPlayerHealth(npcid, 1000);
			EquipArmor(npcid, "ItAr_Kdf_L");
			local x,y,z = GetPlayerPos(playerid);
			SetPlayerPos(npcid,x+50,y,z+50);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /createnpc <Name>");
		end
	elseif cmd == "/spawnmonster" then
		local valid, instance = sscanf(params, "s");
		if valid == 1 then
			local monsterid = CreateNPC(instance);
			SetPlayerInstance(monsterid, instance);
			SetPlayerStrength(monsterid, 50);
			SetPlayerMaxHealth(monsterid, 10000);
			SetPlayerHealth(monsterid, GetPlayerMaxHealth(monsterid));
			SendMessageToAll(255,255,255,"Monster "..monsterid.." ("..instance..") spawned.");
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /spawnmonster <instance>");
		end
	elseif cmd == "/respawn" then
		local focusid = GetFocus(playerid);
		local x,y,z = GetPlayerPos(focusid);
		local valid, id = sscanf(params, "d");
		if valid == 0 and focusid ~= -1 then
			SendPlayerMessage(playerid, 255,255,255, "Usage: /respawn <id> or focus an NPC.");
			return;
		end
		if focusid ~= -1 then
			id = focusid;
		end
		SetPlayerHealth(id, GetPlayerMaxHealth(id));
		SetPlayerMana(id, GetPlayerMaxMana(id));
		SpawnPlayer(id);
		SetPlayerPos(id, x, y+100, z);
		PlayAnimation(id, "S_RUN");
	elseif cmd == "/destroynpc" then
		local valid, npc = sscanf(params, "s");
		if valid == 1 then
			local npcid = DestroyNPC(npc);
			SendPlayerMessage(playerid, 255,255,255, "NPC "..npcid.." destroyed.");
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /destroynpc <Name>");
		end
	elseif cmd == "/isnpc" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			if IsNPC(id) == 1 then
				SendPlayerMessage(playerid, 255,255,255, GetPlayerName(id).." is an npc.");
			else
				SendPlayerMessage(playerid, 255,255,255, GetPlayerName(id).." is NOT an npc.");
			end
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /isnpc <id>");
		end
	-- TIMER --
	elseif cmd == "/settimer" then
			local aParams = params.split(' ');
			local func, delay, rep, more = aParams[1], tonumber(aParams[2]), tonumber(aParams[3]), aParams[4];
			local timerid = SetTimer(func, delay, rep, more);
			if type(timerid) ~= "number" then
				SendPlayerMessage(playerid, 255, 0, 0, "Timer not started!");
			elseif type(timerid) == "number" then
				SendMessageToAll(255,255,255,"Timer "..timerid.." started.");
			else
				SendPlayerMessage(playerid, 255,0,0, "Usage: /settimer <func> <delay> <repeat(bool)> <more>");
			end
	elseif cmd == "/killtimer" then
			local valid, timerid = sscanf(params, "d");
			if valid == 1 then
				if IsTimerActive(timerid) == 1 then
					KillTimer(timerid);
					SendPlayerMessage(playerid, 255,255,255, "Timer "..timerid.." killed.");
				else
					SendPlayerMessage(playerid, 255,255,0, "Timer "..timerid.." was not found.");
				end
			else
				SendPlayerMessage(playerid, 255,0,0, "Usage: /killtimer <id>");
			end
	elseif cmd == "/istimeractive" then
			local valid, timerid = sscanf(params, "d");
			if valid == 1 then
				local b = IsTimerActive(timerid);
				SendPlayerMessage(255,255,255, "Timer "..timerid.." status: "..b);
			else
				SendPlayerMessage(playerid, 255,0,0, "Usage: /istimeractive <id>");
			end
	-- DRAW -- TODO
	-- PLAYERDRAW -- TODO
	-- TEXTURE -- TODO
	-- CURSOR -- see onplayerkey/onplayermouse
	-- SOUND -- TODO
	-- ITEM --
	elseif cmd == "/giveall" then
		local valid, category = sscanf(params, "s");
		if valid == 1 then
			local t = _G[category];
			if not(t) then
				SendPlayerMessage(playerid, 255,255,255, "Category not found.");
				return;
			end
			for i=1, #t do
				GiveItem(playerid, t[i], 1);
			end
			SendPlayerMessage(playerid, 255,255,255, "Gave "..#t.." "..category);
		else
			SendPlayerMessage(playerid, 255,0,0,
			"Use: /giveall <Amulets|AnimalTropy|Armor|FakeScrolls|Food|Keys|\
			MeleeWeapons|Misc|Plants|Potions|RangedWeapons|Rings|Runes|Scrolls|\
			Secrets|Torches|Written");
		end
	elseif cmd == "/destroyitem" then
		local valid, itemid = sscanf(params, "d");
		if valid == 1 then
			DestroyItem(itemid);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /destroyitem <itemid>");
		end
	elseif cmd == "/giveitem" then
		local valid, id, instance, amount = sscanf(params, "dsd");
		if valid == 1 then
			GiveItem(id, instance, amount);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /giveitem <id> <iteminstance> <amount>");
		end
	elseif cmd == "/equipitem" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			EquipItem(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /equipitem <id> <iteminstance>");
		end
	elseif cmd == "/unequipitem" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			UnequipItem(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /unequipitem <id> <iteminstance>");
		end
	elseif cmd == "/removeitem" then
		local valid, id, instance, amount = sscanf(params, "dsd");
		if valid == 1 then
			RemoveItem(id, instance, amount);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /removeitem <id> <iteminstance> <amount>");
		end
	elseif cmd == "/advremoveitem" then
		local valid, id, instance, amount = sscanf(params, "dsd");
		if valid == 1 then
			AdvRemoveItem(id, instance, amount);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /advremoveitem <id> <iteminstance> <amount>");
		end
	elseif cmd == "/dropitem" then
		local valid, id, instance, amount = sscanf(params, "dsd");
		if valid == 1 then
			DropItem(id, instance, amount);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /dropitem <id> <iteminstance> <amount>");
		end
	elseif cmd == "/getplayeritem" then
		local valid, id, slot = sscanf(params, "dd");
		if valid == 1 then
			GetPlayerItem(id,slot);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayeritem <id> <slot>");
		end
	elseif cmd == "/getworlditem" then
		local valid, itemid = sscanf(params, "d");
		if valid == 1 then
			local tString = GetWorldItem(itemid);
			local str = "";
			for i=1, #tString do
				str = str .. tString[i];
			end
			SendPlayerMessage(playerid, 255,255,255, "Item "..str.." has ID: "..itemid);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getworlditem <id>");
		end
	elseif cmd == "/getallequipped" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
		    local ar = GetEquippedArmor(id);
		    local mw = GetEquippedMeleeWeapon(id);
		    local rw = GetEquippedRangedWeapon(id);
		    local he = GetEquippedHelmet(id);
		    local be = GetEquippedBelt(id);
			SendPlayerMessage(playerid,255,255,255,
			string.format("AR: %s, MW: %s, RW: %s, HE: %s, BE: %s", ar,mw,rw,he,be));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getallequipped <id>");
		end
	-- VOB -- TODO
	elseif cmd == "/vobcreate" then
		local valid, visual = sscanf(params, "s");
		if valid == 1 then
			local x,y,z = GetPlayerPos(playerid);
			local world = GetPlayerWorld(playerid);
			local usrdata = Vob.Create(visual,world,x+50,y-90,z+50);
			table.insert(SpawnedVobs, usrdata);
			SendPlayerMessage(playerid, 255,255,255, "Vob "..Vob.GetGUID(usrdata).." with ID: "..#SpawnedVobs.." created.");
		else
			SendPlayerMessage(playerid, 255,0,0, "Use: /vobcreate <visual.3ds>");
		end
	elseif cmd == "/vobsetvisual" then
		local valid, id, visual = sscanf(params, "ds");
		if valid == 1 then
			Vob.SetVisual(SpawnedMobs[id], visual);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Visual set to: %s", visual));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /vobsetvisual <id> <visual.3ds>");
		end
	elseif cmd == "/vobsetvisible" then
		local valid, id, vis = sscanf(params, "dd");
		if valid == 1 then
			Vob.SetVisible(SpawnedVobs[id], vis);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Visible set to: %d", vis));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /vobsetvisible <id> <0|1>");
		end
	elseif cmd == "/vobdestroy" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			Vob.Destroy(VpawnedMobs[id]);
			SpawnedVobs[id] = nil;
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /vobdestroy <id>");
		end
	elseif cmd == "/vobdestroyall" then
		Vob.DestroyAllVobs();
		SendPlayerMessage(playerid, 255,255,255, "All "..#SpawnedVobs.." Vobs destroyed!");
		for i=1, #SpawnedVobs do
			SpawnedVobs[i] = nil;
		end
	-- MOB -- TODO
	elseif cmd == "/mobcreate" then
		local valid, visual, name, mobtype, scheme, useWithItem, objectName = sscanf(params, "ssdsss");
		if valid == 1 then
			local x,y,z = GetPlayerPos(playerid);
			local world = GetPlayerWorld(playerid);
			local usrdata = Mob.Create(visual,name,mobtype,scheme,useWithItem,world,x+50,y,z+50,objectName);
			table.insert(SpawnedMobs, usrdata);
			SendPlayerMessage(playerid, 255,255,255, "Mob "..Mob.GetGUID(usrdata).." with ID: "..#SpawnedMobs.." created.");
		else
			SendPlayerMessage(playerid, 255,0,0,
			"Use: /mobcreate <visual.3ds> <name> <mobtype(int)> <scheme> <usewithitem> <objectname>");
		end
	elseif cmd == "/mobcreateall" then
		mobCreateAll(playerid, params);
	elseif cmd == "/mobsetname" then
		local valid, id, name = sscanf(params, "s");
		if valid == 1 then
			Mob.SetName(SpawnedMobs[id], name);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Name set to: %s", name));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetname <id> <newname>");
		end
	elseif cmd == "/mobsetobjectname" then
		local valid, id, obj = sscanf(params, "s");
		if valid == 1 then
			Mob.SetVisual(SpawnedMobs[id], obj);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Object set to: %s", obj));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetobjectname <id> <objectname>");
		end
	elseif cmd == "/mobsetvisual" then
		local valid, id, visual = sscanf(params, "ds");
		if valid == 1 then
			Mob.SetVisual(SpawnedMobs[id], visual);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Visual set to: %s", visual));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetvisual <id> <visual.3ds>");
		end
	elseif cmd == "/mobsetscheme" then
		local valid, id, mobtype, scheme = sscanf(params, "dds");
		if valid == 1 then
			Mob.SetScheme(SpawnedMobs[id], mobtype, scheme);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Scheme set to: %s", visual));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetscheme <id> <mobtype(int)> <scheme>");
		end
	elseif cmd == "/mobsetusewithitem" then
		local valid, id, uwi = sscanf(params, "ds");
		if valid == 1 then
			Mob.SetUseWithItem(SpawnedMobs[id], uwi);
			SendPlayerMessage(playerid,255,255,255,
			string.format("UseWithItem set to: %s", uwi));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetusewithitem <id> <iteminstance>");
		end
	elseif cmd == "/mobsetvisible" then
		local valid, id, coll = sscanf(params, "dd");
		if valid == 1 then
			Mob.SetVisible(SpawnedMobs[id], coll);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Visible set to: %d", coll));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetvisible <id> <0|1>");
		end
	elseif cmd == "/mobsetcollision" then
		local valid, id, coll = sscanf(params, "dd");
		if valid == 1 then
			Mob.SetCollision(SpawnedMobs[id], coll);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Collision set to: %d", coll));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetcollision <id> <0|1>");
		end
	elseif cmd == "/mobsetposition" then
		local valid,id,x,y,z = sscanf(params, "dfff");
		if valid == 1 then
			Mob.SetPosition(SpawnedMobs[id],x,y,z);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Position set to: %f %f %f",x,y,z));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetposition <id> <x> <y> <z>");
		end
	elseif cmd == "/mobsetrotation" then
		local valid,id,x,y,z = sscanf(params, "dfff");
		if valid == 1 then
			Mob.SetRotation(SpawnedMobs[id],x,y,z);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Rotation set to: %f %f %f",x,y,z));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobsetrotation <id> <rot_x> <rot_y> <rot_z>");
		end
	elseif cmd == "/mobdestroy" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			Mob.Destroy(SpawnedMobs[id]);
			SpawnedMobs[id] = nil;
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /mobdestroy <id>");
		end
	elseif cmd == "/mobdestroyall" then
		Mob.DestroyAllMobs();
		for i=1, #SpawnedMobs do
			SpawnedMobs[i] = nil;
		end
	-- INVENTORY --
	elseif cmd == "/clearinventory" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			ClearInventory(id);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /clearinventory");
		end
	-- CAMERA --
	elseif cmd == "/setcamerabehindplayer" then
		local valid, p1, p2 = sscanf(params, "dd");
		if valid == 1 then
			SetCameraBehindPlayer(p1, p2);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setcamerabehindplayer <id> <id>");
		end
	elseif cmd == "/setcamerabehindvob" then
		local valid, id, vobid = sscanf(params, "dd");
		if valid == 1 then
			SetCameraBehindVob(id, SpawnedVobs[vobid]);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setcamerabehindvob <id> <vobid>");
		end
	elseif cmd == "/setdefaultcamera" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SetDefaultCamera(id);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setdefaultcamera <id>");
		end
	elseif cmd == "/freezecamera" then
		local valid, id, toggle = sscanf(params, "dd");
		if valid == 1 then
			FreezeCamera(id, toggle);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /freezecamera <id> <0|1>");
		end
	-- WALKMODE --
	elseif cmd == "/setplayerwalk" then
		local valid, id, walkstyle = sscanf(params, "ds");
		if valid == 1 then
			SetPlayerWalk(id, walkstyle);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerwalk <id> <walkstyle.mds>");
			SendPlayerMessage(playerid, 255,255,255, "Example: /setplayerwalk 0 humans_militia.mds");
		end
	elseif cmd == "/getplayerwalk" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local str = GetPlayerWalk(id);
			SendPlayerMessage(playerid, 255,255,255, str);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerwalk <id>");
		end
	elseif cmd == "/removeplayeroverlay" then
		local valid, id, overlay = sscanf(params, "ds");
		if valid == 1 then
			RemovePlayerOverlay(id, overlay);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /removeplayeroverlay <id> <overlay>");
		end
	-- ANIMATION --
	elseif cmd == "/playanimation" then
		local valid, id, anim = sscanf(params, "ds");
		if valid == 1 then
			PlayAnimation(id, string.upper(anim));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /playanimation <id> <animation>");
			SendPlayerMessage(playerid, 255,255,255, "Example: /playanimation 0 S_DEADB");
		end
	-- WORLD --
	elseif cmd == "/openlocks" then
		OpenLocks(1);
	elseif cmd == "/closelocks" then
		OpenLocks(0);
	elseif cmd == "/settime" then
		local valid, h, m = sscanf(params, "dd");
		if valid == 1 then
			SetTime(h,m);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /settime <hour> <minute>");
		end
	elseif cmd == "/setweather" then
		local valid, weather, lightning, startHour, startMinute, endHour, endMinute = sscanf(params, "dddddd");
		if valid == 1 then
			SetWeather(weather,lightning,startHour,startMinute,endHour,endMinute);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setweather <weather> <lightning> <startHour> <startMinute> \
                                                  <endHour> <endMinute>");
			SendPlayerMessage(playerid, 255,255,255, "Example: /setweather 1 0 13 37 14 11");
		end
	elseif cmd == "/setplayervirtualworld" then
		local valid, id, world = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerVirtualWorld(id, world);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayervirtualworld <id> <world>");
		end
	elseif cmd == "/getplayervirtualworld" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local vworld = GetPlayerVirtualWorld(id);
			SendPlayerMessage(playerid,255,255,255, vworld);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayervirtualworld <id>");
		end
	elseif cmd == "/setvirtualworldforallconnected" then
		local valid, vworld = sscanf(params, "d");
		if valid == 1 then
			SetVirtualWorldForAllConnected(vworld);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setvirtualworldforallconnected <virtualworld>");
		end
	-- SERVER --
	elseif cmd == "/shutdown" then
		os.exit(0);
	elseif cmd == "/restart" then
		os.exit(1);
	elseif cmd == "/kick" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			Kick(id);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /kick <id>");
		end
	elseif cmd == "/ban" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			Ban(id);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /ban <id>");
		end
	elseif cmd == "/sendplayermessage" then
		local valid, id, msg = sscanf(params, "ds");
		if valid == 1 then
			SendPlayerMessage(playerid, 0,255,255, "You sent "..id..": "..msg);
			SendPlayerMessage(id, 0,255,255, playerid.." sends: "..msg);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /sendplayermessage <id> <msg>");
		end
	elseif cmd == "/sendmessagetoall" then
		local valid, global = sscanf(params, "s");
		if valid == 1 then
			SendMessageToAll(0,255,0, global);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /sendmessagetoall <global>");
		end
	-- LIMITS --
	-- see "/"
	-- MENU --
	elseif cmd == "/setnicknamefont" then
		local valid, font = sscanf(params, "s");
		if valid == 1 then
			SetNicknameFont(font);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setnicknamefont <font>");
		end
	-- UTILS --
	elseif cmd == "/gettickcount" then
		SendPlayerMessage(playerid, 255,255,255, "GetTickCount: "..GetTickCount());
	elseif cmd == "/getdistance2d" then
		local valid, x1, z1, x2, z2 = sscanf(params, "ffff");
		if valid == 1 then
			local distance = GetDistance2D(x1,z1,x2,z2);
			local msg = string.format("Distance between %f,%f and %f,%f: %f",x1,z1,x2,z2,distance);
			SendPlayerMessage(playerid, 255,255,255, msg);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getdistance2d <x1> <z1> <x2> <z2>");
		end
	elseif cmd == "/getdistance3d" then
		local valid, x1, y1, z1, x2, y2, z2 = sscanf(params, "ffffff");
		if valid == 1 then
			local distance = GetDistance2D(x1,y1,z1,x2,y2,z2);
			local msg = string.format("Distance between %f,%f,%f and %f,%f,%f: %f",x1,y1,z1,x2,y2,z2,distance);
			SendPlayerMessage(playerid, 255,255,255, msg);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getdistance3d <x1> <y1> <z1> <x2> <y2> <z2>");
		end
	elseif cmd == "/getdistanceplayers" then
		local valid, p1, p2 = sscanf(params, "dd");
		if valid == 1 then
			local distance = GetDistancePlayers(p1,p2);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Distance between %s and %s: %f", GetPlayerName(p1), GetPlayerName(p2), distance));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getdistanceplayers <id> <id>");
		end
	elseif cmd == "/getangletopos" then
		local valid, x1,z1,x2,z2 = sscanf(params, "ffff");
		if valid == 1 then
			local angle = GetAngleToPos(x1,z1,x2,z2);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Angle for pos x:%f z:%f and pos x:%f z:%f: %d", x1,z1,x2,z2,angle));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getangletopos <x1> <z1> <x2> <z2>");
		end
	elseif cmd == "/getplayerangleto" then
		local valid,id,x,z = sscanf(params, "dff");
		if valid == 1 then
			local angle = GetPlayerAngleTo(id,x,z);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Angle for player %s: %d", GetPlayerName(id),angle));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerangleto <id> <x> <z>");
		end
	elseif cmd == "/completeheal" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			CompleteHeal(id);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /completeheal <id>");
		end
	-- PLAYER --
	elseif cmd == "/setplayercolor" then
		local valid, id, r,g,b = sscanf(params, "dddd");
		if valid == 1 then
			SetPlayerColor(id, r, g, b);
			SendPlayerMessage(id, r, g, b, "This message displays your new playercolor.");
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayercolor <id> <r> <g> <b>");
		end
	elseif cmd == "/getplayercolor" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local r,g,b = GetPlayerColor(id);
			SendPlayerMessage(playerid, r,g,b, "Message is colored in "..GetPlayerName(id).."'s playercolor.");
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayercolor <id>");
		end
	elseif cmd == "/setplayermaxhealth" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerMaxHealth(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayermaxhealth <id> <value>");
		end
	elseif cmd == "/getplayermaxhealth" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SendPlayerMessage(playerid, 255,255,255, GetPlayerMaxHealth(id));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayermaxhealth <id>");
		end
	elseif cmd == "/setplayerhealth" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerHealth(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerhealth <id> <value>");
		end
	elseif cmd == "/getplayerhealth" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SendPlayerMessage(playerid, 255,255,255, GetPlayerHealth(id));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerhealth <id>");
		end
	elseif cmd == "/setplayermaxmana" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerMaxMana(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayermaxmana <id> <value>");
		end
	elseif cmd == "/getplayermaxmana" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SendPlayerMessage(playerid, 255,255,255, GetPlayerMaxMana(id));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayermaxmana <id>");
		end
	elseif cmd == "/setplayermana" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerMana(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayermana <id> <value>");
		end
	elseif cmd == "/getplayermana" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SendPlayerMessage(playerid, 255,255,255, GetPlayerMana(id));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayermana <id>");
		end
	elseif cmd == "/setplayerstrength" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerStrength(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerstrength <id> <value>");
		end
	elseif cmd == "/getplayerstrength" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SendPlayerMessage(playerid, 255,255,255, GetPlayerStrength(id));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerstrength <id>");
		end
	elseif cmd == "/setplayerdexterity" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerDexterity(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerdexterity <id> <value>");
		end
	elseif cmd == "/getplayerdexterity" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SendPlayerMessage(playerid, 255,255,255, GetPlayerDexterity(id));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerdexterity <id>");
		end
	elseif cmd == "/setplayerpos" then
		local valid, id, x,y,z = sscanf(params, "dfff");
		if valid == 1 then
			SetPlayerPos(id,x,y,z);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerpos <id> <x> <y> <z>");
		end
	elseif cmd == "/getplayerpos" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local x,y,z = GetPlayerPos(id);
			SendPlayerMessage(playerid, 255,255,255,
			string.format("%s is at %f;%f;%f",GetPlayerName(id),x,y,z));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerpos <id>");
		end
	elseif cmd == "/setplayerangle" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerAngle(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerangle <id> <value>");
		end
	elseif cmd == "/getplayerangle" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local angle = GetPlayerAngle(id);
			SendPlayerMessage(playerid, 255,255,255,
			string.format("%s's angle: %d",GetPlayerName(id),angle));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerangle <id>");
		end
	elseif cmd == "/setplayername" then
		local valid, id, name = sscanf(params, "ds");
		if valid == 1 then
			SetPlayerName(id, name);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayername <id> <name>");
		end
	elseif cmd == "/getplayername" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local name =GetPlayerName(id);
			SendPlayerMessage(playerid, 255,255,255, "Name: "..name);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayername <id>");
		end
	elseif cmd == "/setplayerinstance" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			SetPlayerInstance(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerinstance <id> <instance>");
		end
	elseif cmd == "/getplayerinstance" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local instance = GetPlayerInstance(id);
			SendPlayerMessage(playerid, 255,255,255, "Instance: "..instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerinstance <id>");
		end
	elseif cmd == "/setplayerskillweapon" then
		local valid, id, skill, value = sscanf(params, "dds");
		if valid == 1 then
			SetPlayerSkillWeapon(id, skill, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerskillweapon <id> <skill> <value>");
		end
	elseif cmd == "/getplayerskillweapon" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local skill = GetPlayerSkillWeapon(id);
			SendPlayerMessage(playerid, 255,255,255, "Skill: "..skill);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerskillweapon <id>");
		end
	elseif cmd == "/setplayerweaponmode" then
		local valid, id, mode = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerWeaponMode(id, mode);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerweaponmode <id> <mode>");
		end
	elseif cmd == "/getplayerweaponmode" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local mode = GetPlayerWeaponMode(id);
			SendPlayerMessage(playerid, 255,255,255, "Weaponmode: "..mode);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerweaponmode <id>");
		end
	elseif cmd == "/setplayeradditionalvisual" then
		local valid, id, bodyModel, bodyTextureID, headModel, headTextureID = sscanf(params, "dsdsd");
		if valid == 1 then
			SetPlayerAdditionalVisual(id,bodyModel,bodyTextureID,headModel,headTextureID);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayeradditionalvisual \
                              <id> <bodymodel> <bodytexid> <headmodel> <headtexid>");
			SendPlayerMessage(playerid, 255,255,255, "Example: /setplayeradditionalvisual \
                              0 hum_body_naked 7 hum_head_fatbald 4");
		end
	elseif cmd == "/getplayeradditionalvisional" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local bodyModel,bodyTextureID,headModel,headTextureID = GetPlayerAdditionalVisual(id);
			SendPlayerMessage(playerid, 255,255,255, string.format("AddVisual for %s: %s %d %s %d ",
			GetPlayerName(id),bodyModel,bodyTextureID,headModel,headTextureID));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayeradditionalvisual <id>");
		end
	elseif cmd == "/setplayerfatness" then
		local valid, id, value = sscanf(params, "df");
		if valid == 1 then
			SetPlayerFatness(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerfatness <id> <value>");
		end
	elseif cmd == "/getplayerfatness" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local fatness = GetPlayerFatness(id);
			SendPlayerMessage(playerid, 255,255,255, "Fatness: "..fatness);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerfatness <id>");
		end
	elseif cmd == "/setplayeracrobatic" then
		local valid, id, value = sscanf(params, "df");
		if valid == 1 then
			SetPlayerAcrobatic(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayeracrobatic <id> <0|1>");
		end
	elseif cmd == "/getplayeracrobatic" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local acrobatic = GetPlayerAcrobatic(id);
			SendPlayerMessage(playerid, 255,255,255, "Acrobatic: "..acrobatic);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayeracrobatic <id>");
		end
	elseif cmd == "/setplayerlevel" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerLevel(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerlevel <id> <value>");
		end
	elseif cmd == "/getplayerlevel" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local lvl = GetPlayerLevel(id);
			SendPlayerMessage(playerid, 255,255,255, "Level: "..lvl);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerlevel <id>");
		end
	elseif cmd == "/setplayerexperience" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerExperience(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerexperience <id> <value>");
		end
	elseif cmd == "/getplayerexperience" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local exp = GetPlayerExperience(id);
			SendPlayerMessage(playerid, 255,255,255, "Experience: "..exp);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerexperience <id>");
		end
	elseif cmd == "/setplayerexperiencenextlevel" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerExperienceNextLevel(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerexperiencenextlevel <id> <value>");
		end
	elseif cmd == "/getplayerexperiencenextlevel" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local exp = GetPlayerExperienceNextLevel(id);
			SendPlayerMessage(playerid, 255,255,255, "Next Level Experience: "..exp);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerexperiencenextlevel <id>");
		end
	elseif cmd == "/setplayerlearnpoints" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerLearnPoints(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerlearnpoints <id> <value>");
		end
	elseif cmd == "/getplayerlearnpoints" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local lp = GetPlayerLearnPoints(id);
			SendPlayerMessage(playerid, 255,255,255, "Learn points: "..lp);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerlearnpoints <id>");
		end
	elseif cmd == "/freezeplayer" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			FreezePlayer(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /freezeplayer <id> <0|1>");
		end
	elseif cmd == "/equiparmor" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			EquipArmor(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /equiparmor <id> <ItAr_...>");
		end
	elseif cmd == "/unequiparmor" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			UnequipArmor(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /unequiparmor <id> <ItAr_...>");
		end
	elseif cmd == "/equiphelmet" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			EquipHelmet(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /equiphelmet <id> <ItAr_...>");
		end
	elseif cmd == "/unequiphelmet" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			UnequipHelmet(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /unequiphelmet <id> <ItAr_...>");
		end
	elseif cmd == "/equipbelt" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			EquipBelt(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /equipbelt <id> <ItBe_...>");
		end
	elseif cmd == "/unequipbelt" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			UnequipBelt(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /unequipbelt <id> <ItBe_...>");
		end
	elseif cmd == "/equipmeleeweapon" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			EquipMeleeWeapon(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /equipmeleeweapon <id> <ItMw_...>");
		end
	elseif cmd == "/unequipmeleeweapon" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			UnequipMeleeWeapon(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /unequipmeleeweapon <id> <ItMw_...>");
		end
	elseif cmd == "/equiprangedweapon" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			EquipRangedWeapon(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /equiprangedweapon <id> <ItRw_...>");
		end
	elseif cmd == "/unequiprangedweapon" then
		local valid, id, instance = sscanf(params, "ds");
		if valid == 1 then
			UnequipRangedWeapon(id, instance);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /unequiprangedweapon <id> <ItRw_...>");
		end
	elseif cmd == "/setplayerworld" then
		local valid, id, world, wp = sscanf(params, "dss");
		if valid == 1 then
			SetPlayerWorld(id, world, wp);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerworld <id> <world> <waypoint>");
		end
	elseif cmd == "/getplayerworld" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local world = GetPlayerWorld(id);
			SendPlayerMessage(playerid, 255,255,255, GetPlayerName(id).."'s World: "..world);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerworld <id>");
		end
	elseif cmd == "/setplayermagiclevel" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerMagicLevel(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayermagiclevel <id> <0-6>");
		end
	elseif cmd == "/getplayermagiclevel" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local ml = GetPlayerMagicLevel(id);
			SendPlayerMessage(playerid, 255,255,255, "Magic level: "..ml);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayermagiclevel <id>");
		end
	elseif cmd == "/setplayerscience" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerScience(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerscience <id> <value>");
		end
	elseif cmd == "/getplayerscience" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local sc = GetPlayerScience(id);
			SendPlayerMessage(playerid, 255,255,255, "Science: "..sc);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerscience <id>");
		end
	elseif cmd == "/setplayergold" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerGold(id, value);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayergold <id> <value>");
		end
	elseif cmd == "/getplayergold" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local gold = GetPlayerGold(id);
			SendPlayerMessage(playerid, 255,255,255, "Gold: "..gold);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayergold <id>");
		end
	elseif cmd == "/getfocus" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local focusid = GetFocus(id);
			SendPlayerMessage(playerid, 255,255,255, "Focus: "..focusid);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getfocus <id>");
		end
	elseif cmd == "/getplayerip" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local ipaddr = GetPlayerIP(id);
			SendPlayerMessage(playerid,255,255,255,
			string.format("IP Address: %s", ipaddr));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerip <id>");
		end
	elseif cmd == "/getmacaddress" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local addr = GetMacAddress(id);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Mac Address: %s", addr));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getmacaddress <id>");
		end
	elseif cmd == "/enabledropafterdeath" then
		local valid, toggle = sscanf(params, "d");
		if valid == 1 then
			EnableDropAfterDeath(toggle);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /enabledropafterdeath <0|1>");
		end
	elseif cmd == "/getlefthand" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local item = GetLeftHand(id, hand);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Item in left hand: %s", item));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getlefthand <id>");
		end
	elseif cmd == "/getrighthand" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local item = GetRightHand(id, hand);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Item in right hand: %s", item));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getrighthand <id>");
		end
	elseif cmd == "/setplayerscale" then
		local valid, id, x, y, z = sscanf(params, "dfff");
		if valid == 1 then
			SetPlayerScale(id, x, y, z);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerscale <id> <length> <width> <height>");
		end
	elseif cmd == "/getplayerscale" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local x,y,z = GetPlayerScale(id);
			SendPlayerMessage(playerid, 255,255,255, "Scale: "..x.." "..y.." "..z);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerscale <id>");
		end
	elseif cmd == "/enablemarvin" then
		ShowTexture(playerid, Textures["CONSOLE"]);
		EnableMarvin(playerid, 1);
		SendPlayerMessage(playerid, 255,255,255, "Marvin mode activated.");
	elseif cmd == "/disablemarvin" then
		HideTexture(playerid, Textures["CONSOLE"]);
		EnableMarvin(playerid, 0);
		SendPlayerMessage(playerid, 255,255,255, "Marvin mode deactivated.");
	elseif cmd == "/setbleedthreshold" then
		local valid, threshold = sscanf(params, "f");
		if valid == 1 then
			SetBleedThreshold(threshold);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setbleedthreshold <value>");
		end
	elseif cmd == "/turnplayerto" then
		local valid, id, x, z = sscanf(params, "dff");
		if valid == 1 then
			TurnPlayerTo(id,x,z);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /turnplayerto <id> <x> <z>");
		end
	elseif cmd == "/setplayerguild" then
		local valid, id, guildid = sscanf(params, "dd");
		if valid == 1 then
			SetPlayerGuild(id, guildid);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerguild <id> <guildid>");
		end
	elseif cmd == "/getplayerresolution" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			local res_x,res_y = GetPlayerResolution(id);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Resolution: %d x %d", res_x,res_y));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerresolution <id>");
		end
	elseif cmd == "/getplayerhand" then
		local valid, id, hand = sscanf(params, "dd");
		if valid == 1 then
			local item = GetPlayerHand(id, hand);
			SendPlayerMessage(playerid,255,255,255,
			string.format("Item in hand %d: %s", hand, item));
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /getplayerhand <id>");
		end
	elseif cmd == "/setplayerhand" then
		local valid, id, hand, item = sscanf(params, "dds");
		if valid == 1 then
			SetPlayerHand(id, hand, item);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /setplayerhand <id> <hand: 0|1> <iteminstance>");
		end
	elseif cmd == "/spawnplayer" then
		local valid, id = sscanf(params, "d");
		if valid == 1 then
			SpawnPlayer(id);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /spawnplayer <id>");
		end
	-- MISC --
	elseif cmd == "/help" then
		SendPlayerMessage(playerid,255,255,255, "Type /instant80");
	elseif cmd == "/instant80" then
		initInstantMode(playerid);
	elseif cmd == "/healovertime" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			Player[id].hot = SetTimer("xOverTime", 2100, 1, id, "+", value);
			SendPlayerMessage(id,255,255,255, "HOT Timer started, ID: "..Player[id].hot);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /healovertime <id> <value>");
		end
	elseif cmd == "/dmgovertime" then
		local valid, id, value = sscanf(params, "dd");
		if valid == 1 then
			Player[id].dot = SetTimer("xOverTime", 2000, 1, id, "-", value);
			SendPlayerMessage(id,255,255,255, "DOT Timer started, ID: "..Player[id].dot);
		else
			SendPlayerMessage(playerid, 255,0,0, "Usage: /dmgovertime <id> <value>");
		end
	elseif cmd == "/reloadscripts" then
		big_require("include", true,"*");
	elseif cmd == "/q" or cmd == "/quit" or cmd == "/exit" then
		ExitGame(playerid);
	else
		SendPlayerMessage(playerid, 255, 0, 0, "pts: "..cmd..": command not found");
		return;
	end
	SendPlayerMessage(playerid, 0,200,0, "You issued: "..cmdtext);
end

print(debug.getinfo(1).source .. " has been loaded.");
